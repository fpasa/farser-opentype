use super::*;

#[derive(Debug, Default)]
pub struct FontCollection {
    pub fonts: Vec<Font>,
}

impl FontCollection {
    /// Return first font in collection.
    ///
    /// This function provides a convenience when dealing with TTF and OTF font files,
    /// which contain just one font.
    pub fn first(&self) -> &Font {
        // At least one font is guaranteed to exist, otherwise
        // an error would be returned while parsing.
        self.fonts.first().unwrap()
    }
}
