use super::*;

#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub struct Coordinate {
    pub x: i32,
    pub y: i32,
}

#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub struct Contour {
    pub points: Vec<Coordinate>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub struct Glyph {
    pub bounds: Bounds,
    pub contours: Vec<Contour>,
}
