use super::*;

// pub(crate) enum Platform {
//     Unicode,
//     Macintosh,
//     ISO,
//     Windows,
//     Custom,
// }

#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) enum UnicodeEncoding {
    Unicode1,
    Unicode1_1,
    ISO_IEC_10646,
    Unicode2BMP,
    Unicode2FullRepertoire,
    UnicodeVariationSequences,
    UnicodeFullRepertoire,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) enum MacintoshEncoding {
    Roman,
    Japanese,
    TraditionalChinese,
    Korean,
    Arabic,
    Hebrew,
    Greek,
    Russian,
    RSymbol,
    Devanagari,
    Gurmukhi,
    Gujarati,
    Oriya,
    Bengali,
    Tamil,
    Telugu,
    Kannada,
    Malayalam,
    Sinhalese,
    Burmese,
    Khmer,
    Thai,
    Laotian,
    Georgian,
    Armenian,
    SimplifiedChinese,
    Tibetan,
    Mongolian,
    Geez,
    Slavic,
    Vietnamese,
    Sindhi,
    Uninterpreted,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) enum ISOEncoding {
    ASCII,
    ISO_10646,
    ISO_8859_1,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) enum WindowsEncoding {
    Symbol,
    UnicodeBMP,
    ShiftJIS,
    PRC,
    Big5,
    Wansung,
    Johab,
    Reserved,
    UnicodeFullRepertoire,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) enum Encoding {
    Unicode(UnicodeEncoding),
    Macintosh(MacintoshEncoding),
    ISO(ISOEncoding),
    Windows(WindowsEncoding),
    Custom(),
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) struct Mapping {
    pub encoding: Encoding,
    pub offset: u32,
}

pub(crate) fn encoding_from_ids(platform_id: u16, encoding_id: u16) -> Result<Encoding, Error> {
    Ok(match platform_id {
        0 => match encoding_id {
            0 => Encoding::Unicode(UnicodeEncoding::Unicode1),
            1 => Encoding::Unicode(UnicodeEncoding::Unicode1_1),
            2 => Encoding::Unicode(UnicodeEncoding::ISO_IEC_10646),
            3 => Encoding::Unicode(UnicodeEncoding::Unicode2BMP),
            4 => Encoding::Unicode(UnicodeEncoding::Unicode2FullRepertoire),
            5 => Encoding::Unicode(UnicodeEncoding::UnicodeVariationSequences),
            6 => Encoding::Unicode(UnicodeEncoding::UnicodeFullRepertoire),
            _ => {
                return Err(Error::ParseError(format!(
                    "Unsupported or invalid encoding ID '{}' for platform '{}'",
                    encoding_id, platform_id,
                )))
            }
        },
        1 => match encoding_id {
            0 => Encoding::Macintosh(MacintoshEncoding::Roman),
            1 => Encoding::Macintosh(MacintoshEncoding::Japanese),
            2 => Encoding::Macintosh(MacintoshEncoding::TraditionalChinese),
            3 => Encoding::Macintosh(MacintoshEncoding::Korean),
            4 => Encoding::Macintosh(MacintoshEncoding::Arabic),
            5 => Encoding::Macintosh(MacintoshEncoding::Hebrew),
            6 => Encoding::Macintosh(MacintoshEncoding::Greek),
            7 => Encoding::Macintosh(MacintoshEncoding::Russian),
            8 => Encoding::Macintosh(MacintoshEncoding::RSymbol),
            9 => Encoding::Macintosh(MacintoshEncoding::Devanagari),
            10 => Encoding::Macintosh(MacintoshEncoding::Gurmukhi),
            11 => Encoding::Macintosh(MacintoshEncoding::Gujarati),
            12 => Encoding::Macintosh(MacintoshEncoding::Oriya),
            13 => Encoding::Macintosh(MacintoshEncoding::Bengali),
            14 => Encoding::Macintosh(MacintoshEncoding::Tamil),
            15 => Encoding::Macintosh(MacintoshEncoding::Telugu),
            16 => Encoding::Macintosh(MacintoshEncoding::Kannada),
            17 => Encoding::Macintosh(MacintoshEncoding::Malayalam),
            18 => Encoding::Macintosh(MacintoshEncoding::Sinhalese),
            19 => Encoding::Macintosh(MacintoshEncoding::Burmese),
            20 => Encoding::Macintosh(MacintoshEncoding::Khmer),
            21 => Encoding::Macintosh(MacintoshEncoding::Thai),
            22 => Encoding::Macintosh(MacintoshEncoding::Laotian),
            23 => Encoding::Macintosh(MacintoshEncoding::Georgian),
            24 => Encoding::Macintosh(MacintoshEncoding::Armenian),
            25 => Encoding::Macintosh(MacintoshEncoding::SimplifiedChinese),
            26 => Encoding::Macintosh(MacintoshEncoding::Tibetan),
            27 => Encoding::Macintosh(MacintoshEncoding::Mongolian),
            28 => Encoding::Macintosh(MacintoshEncoding::Geez),
            29 => Encoding::Macintosh(MacintoshEncoding::Slavic),
            30 => Encoding::Macintosh(MacintoshEncoding::Vietnamese),
            31 => Encoding::Macintosh(MacintoshEncoding::Sindhi),
            32 => Encoding::Macintosh(MacintoshEncoding::Uninterpreted),
            _ => {
                return Err(Error::ParseError(format!(
                    "Unsupported or invalid encoding ID '{}' for platform '{}'",
                    encoding_id, platform_id,
                )))
            }
        },
        2 => match encoding_id {
            0 => Encoding::ISO(ISOEncoding::ASCII),
            1 => Encoding::ISO(ISOEncoding::ISO_10646),
            2 => Encoding::ISO(ISOEncoding::ISO_8859_1),
            _ => {
                return Err(Error::ParseError(format!(
                    "Unsupported or invalid encoding ID '{}' for platform '{}'",
                    encoding_id, platform_id,
                )))
            }
        },
        3 => match encoding_id {
            0 => Encoding::Windows(WindowsEncoding::Symbol),
            1 => Encoding::Windows(WindowsEncoding::UnicodeBMP),
            2 => Encoding::Windows(WindowsEncoding::ShiftJIS),
            3 => Encoding::Windows(WindowsEncoding::PRC),
            4 => Encoding::Windows(WindowsEncoding::Big5),
            5 => Encoding::Windows(WindowsEncoding::Wansung),
            6 => Encoding::Windows(WindowsEncoding::Johab),
            7 | 8 | 9 => Encoding::Windows(WindowsEncoding::Reserved),
            10 => Encoding::Windows(WindowsEncoding::UnicodeFullRepertoire),
            _ => {
                return Err(Error::ParseError(format!(
                    "Unsupported or invalid encoding ID '{}' for platform '{}'",
                    encoding_id, platform_id,
                )))
            }
        },
        4 => Encoding::Custom(),
        _ => {
            return Err(Error::ParseError(format!(
                "Unsupported or invalid platform ID '{}'",
                platform_id
            )))
        }
    })
}
