use super::*;

use chrono::{TimeZone, Utc};
use std::borrow::Borrow;
use std::env;
use std::path::{Path, PathBuf};

fn resource(file: &str) -> PathBuf {
    // Running tests from the root folder is the only supported alternative
    let mut res_dir = env::current_dir().unwrap();
    res_dir.push("resources");
    res_dir.push(file);
    res_dir
}

fn ttf_resource() -> PathBuf {
    resource("SourceSerifPro-Regular.ttf")
}

fn otf_resource() -> PathBuf {
    resource("SourceCodePro-Regular.otf")
}

fn ttc_resource() -> PathBuf {
    resource("NotoSansCJK-Thin.ttc")
}

fn otc_resource() -> PathBuf {
    resource("NotoSansCJK.otc")
}

fn ttf_reader() -> BufReader<File> {
    open_file(ttf_resource()).unwrap()
}

fn otf_reader() -> BufReader<File> {
    open_file(otf_resource()).unwrap()
}

fn ttc_reader() -> BufReader<File> {
    open_file(ttc_resource()).unwrap()
}

fn otc_reader() -> BufReader<File> {
    open_file(otc_resource()).unwrap()
}

fn ttf_collection() -> FontCollection {
    parse(
        ttf_resource(),
        &ParseConfig {
            // Without integrity check, it's much faster
            check_integrity: false,
            print_warnings: true,
        },
    )
    .unwrap()
}

fn ttf_font() -> Font {
    ttf_collection().fonts[0].clone()
}

fn otf_collection() -> FontCollection {
    parse(
        otf_resource(),
        &ParseConfig {
            // Without integrity check, it's much faster
            check_integrity: false,
            print_warnings: true,
        },
    )
    .unwrap()
}

fn otf_font() -> Font {
    otf_collection().fonts[0].clone()
}

fn ttc_collection() -> FontCollection {
    parse(
        ttc_resource(),
        &ParseConfig {
            // Without integrity check, it's much faster
            check_integrity: false,
            print_warnings: true,
        },
    )
    .unwrap()
}

fn otc_collection() -> FontCollection {
    parse(
        otc_resource(),
        &ParseConfig {
            // Without integrity check, it's much faster
            check_integrity: false,
            print_warnings: true,
        },
    )
    .unwrap()
}

#[test]
fn test_read_tag() {
    // Test reading tag by reading the first 4 bytes of each test file.
    // This also shows what the first value in TTF/OTF/TTC/OTC files is, for reference.

    // TTF files start with the offset table, which starts with a tag 0x00010000
    // if they contain TrueType outlines as opposed to CFF ones
    assert!(read_tag(&mut ttf_reader()).unwrap() == tags::TYPE_TRUETYPE);

    // TTF files start with the offset table, which starts with a tag 0x4F54544F
    // if they contain CFF outlines (OTTO tag)
    assert!(read_tag(&mut otf_reader()).unwrap() == tags::TYPE_CFF);

    // TTC and OTC files start with a ttcf tag both for TrueType and CFF outlines
    // since these can be mixed inside of the collections
    assert!(read_tag(&mut ttc_reader()).unwrap() == tags::TYPE_COLLECTION);
    assert!(read_tag(&mut otc_reader()).unwrap() == tags::TYPE_COLLECTION);
}

#[test]
fn parse_wrong_extension() {
    for filename in &["test.woff", "test.eot", "test"] {
        match parse_default(resource(filename)) {
            Err(err) => match err {
                Error::ValueError(_) => {}
                _ => panic!("Should return InvalidFileError, returned: {:?}.", err),
            },
            _ => panic!("Should return an error."),
        }
    }
}

#[test]
fn parse_does_not_exist() {
    match parse_default(resource("does_not_exist.ttf")) {
        Err(err) => match err {
            Error::IOError(_) => {}
            _ => panic!("Should return IOError, returned: {:?}.", err),
        },
        _ => panic!("Should return an error."),
    }
}

#[test]
fn parse_collection() {
    assert_eq!(ttf_collection().fonts.len(), 1);
    assert_eq!(otf_collection().fonts.len(), 1);
    assert_eq!(ttc_collection().fonts.len(), 4);
    assert_eq!(otc_collection().fonts.len(), 36);
}

#[test]
fn parse_ttf_head() {
    let font = ttf_font();
    assert_eq!(font.created, Utc.ymd(2014, 04, 27).and_hms(23, 15, 15));
    assert_eq!(font.modified, Utc.ymd(2014, 04, 28).and_hms(6, 15, 16));
    assert_eq!(
        font.bounds,
        Bounds {
            x_min: -178,
            y_min: -334,
            x_max: 1138,
            y_max: 918,
            width: 1316,
            height: 1252,
        }
    );
    assert_eq!(font.smallest_readable_size_in_px, 9);
    assert_eq!(
        font.font_direction_hint,
        FontDirectionHint::LeftToRightNeutral
    );
}

#[test]
fn parse_ttf_maxp() {
    let font = ttf_font();
    assert_eq!(font.num_glyphs, 547);
    assert_eq!(font.max_points, 112);
    assert_eq!(font.max_contours, 5);
    assert_eq!(font.max_composite_points, 124);
    assert_eq!(font.max_composite_contours, 7);
    assert_eq!(font.max_zones, 1);
    assert_eq!(font.max_twilight_points, 0);
    assert_eq!(font.max_storage, 0);
    assert_eq!(font.max_max_function_defs, 10);
    assert_eq!(font.max_instruction_defs, 0);
    assert_eq!(font.max_stack_elements, 512);
    assert_eq!(font.max_size_of_instruction, 371);
    assert_eq!(font.max_component_elements, 4);
    assert_eq!(font.max_component_depth, 1);
}

// #[test]
// fn parse_ttf_read_simple_glyphs_truetype() {
//     let font = ttf_font();
//     assert_eq!(font.glyps.len() as u16, font.num_glyphs);
//
//     test_glyph(&font.glyps[0], 5, &[2, 3, 3, 3, 4]);
//     test_glyph(&font.glyps[1], 0, &[]);
//     test_glyph(&font.glyps[5], 3, &[10, 11, 31]);
// }

// #[test]
// fn test_glyph(glyph: &Glyph, num_contours: usize, num_countour_points: &[usize]) {
//     println!("Glyph contour number. Expected: {}", num_contours);
//     assert_eq!(glyph.contours.len(), num_contours);
//
//     for (i, num_points) in num_countour_points.iter().enumerate() {
//         println!(
//             "    Glyph contour {} length. Expected: {}",
//             i + 1,
//             num_points
//         );
//         assert_eq!(glyph.contours[i].points.len(), *num_points);
//     }
// }

#[test]
fn parse_ttc_integrity_check() {
    // This test is there just to give an idea of how slow the integrity check is.
    // Answer: very slow for large files.
    parse_default(ttc_resource());
}
