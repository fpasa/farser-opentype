use super::*;
use std::collections::HashSet;

pub(crate) fn check_table_tag(table_tag: &tags::Tag) -> Result<(), Error> {
    for tag in tags::TABLE_TAGS {
        if table_tag == tag {
            return Ok(());
        }
    }
    Ok(())
    // Err(Error::ParseError(format!(
    //     "Table '{}' is not a valid OpenType table.",
    //     str::from_utf8(table_tag).expect("Tag contains invalid UTF8")
    // )))
}

pub(crate) fn check_required_tables(
    font_tag: &tags::Tag,
    table_tags: &HashSet<&tags::Tag>,
) -> Result<(), Error> {
    let check_tags_present = |tags: &[tags::Tag]| {
        for tag in tags {
            if !table_tags.contains(tag) {
                return Err(Error::ParseError(format!(
                    "Font does not contain required table '{}'.",
                    str::from_utf8(tag).expect("Tag contains invalid UTF8")
                )));
            }
        }
        Ok(())
    };

    // Required for all fonts
    check_tags_present(&[
        tags::TABLE_cmap,
        tags::TABLE_head,
        tags::TABLE_hhea,
        tags::TABLE_hmtx,
        tags::TABLE_maxp,
        tags::TABLE_name,
        tags::TABLE_OS_2,
        tags::TABLE_post,
    ])?;

    if font_tag == &tags::TYPE_TRUETYPE {
        // These are required for TrueType outline files
        check_tags_present(&[
            // tags::TABLE_cvt,
            // tags::TABLE_fpgm,
            tags::TABLE_glyf,
            tags::TABLE_loca,
            // tags::TABLE_prep,
        ])?;
    } else if font_tag == &tags::TYPE_CFF {
        // These are required for CFF outline files
        check_tags_present(&[
            tags::TABLE_CFF,
            // tags::TABLE_VORG
        ])?;
    }

    Ok(())
}

pub(crate) fn check_table_integrity(
    reader: &mut (impl BufRead + Seek),
    check_sum: u32,
    offset: u32,
    length: u32,
) -> Result<(), Error> {
    reader.seek(SeekFrom::Start(offset as u64));

    let mut sum: u32 = 0;
    // length might not be a multiple of 4 bytes,
    // so we need to pad the length to the nearest multiple of 4
    for _ in 0..(((length + 3) & !3) / 4) {
        sum = sum.overflowing_add(read_u32(reader)?).0;
    }

    if check_sum != sum {
        return Err(Error::ParseError(
            "Integrity check failed: the font file is probably invalid or corrupt.".to_string(),
        ));
    }

    Ok(())
}
