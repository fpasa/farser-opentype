use super::*;

mod head_table;
pub(crate) use head_table::*;
mod cmap_table;
pub(crate) use cmap_table::*;
mod maxp_table;
pub(crate) use maxp_table::*;
mod loca_table;
pub(crate) use loca_table::*;
// mod glyf_table;
// pub(crate) use glyf_table::*;
