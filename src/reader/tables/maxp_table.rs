use super::*;

pub(crate) fn parse_maxp_table(
    mut reader: &mut (impl BufRead + Seek),
    offset: u32,
    font: &mut Font,
    font_data: &mut ParserFontData,
    config: &ParseConfig,
) -> Result<(), Error> {
    reader.seek(SeekFrom::Start(offset as u64));

    let version = read_tag(&mut reader)?;
    font.num_glyphs = read_u16(&mut reader)?;
    match version {
        [0, 0, 5, 0] => {}
        [0, 1, 0, 0] => {
            font.max_points = read_u16(&mut reader)?;
            font.max_contours = read_u16(&mut reader)?;
            font.max_composite_points = read_u16(&mut reader)?;
            font.max_composite_contours = read_u16(&mut reader)?;
            font.max_zones = read_u16(&mut reader)?;
            font.max_twilight_points = read_u16(&mut reader)?;
            font.max_storage = read_u16(&mut reader)?;
            font.max_max_function_defs = read_u16(&mut reader)?;
            font.max_instruction_defs = read_u16(&mut reader)?;
            font.max_stack_elements = read_u16(&mut reader)?;
            font.max_size_of_instruction = read_u16(&mut reader)?;
            font.max_component_elements = read_u16(&mut reader)?;
            font.max_component_depth = read_u16(&mut reader)?;
        }
        val => {
            return Err(Error::ParseError(format!(
                "Unsupported head table version {:?}.",
                val
            )))
        }
    }

    Ok(())
}
