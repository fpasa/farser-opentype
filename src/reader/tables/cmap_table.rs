use super::*;
use crate::encoding_from_ids;

pub(crate) fn parse_cmap_table(
    mut reader: &mut (impl BufRead + Seek),
    offset: u32,
    font: &mut Font,
    font_data: &mut ParserFontData,
    config: &ParseConfig,
) -> Result<(), Error> {
    reader.seek(SeekFrom::Start(offset as u64));

    let version = read_u16(&mut reader)?;
    if version != 0 {
        return Err(Error::ParseError(format!(
            "Unsupported cmap table version {}.",
            version
        )))?;
    }

    let num_tables = read_u16(&mut reader)?;

    for _ in 0..num_tables {
        let platform_id = read_u16(&mut reader)?;
        let encoding_id = read_u16(&mut reader)?;
        let mapping_offset = read_u32(&mut reader)?;

        println!("plat: {} enc: {}", platform_id, encoding_id);

        let encoding = encoding_from_ids(platform_id, encoding_id)?;
        if let Encoding::ISO(_) = encoding {
            font.add_warning("Deprecated ISO encoding found", config);
        }

        font.add_mapping(Mapping {
            encoding,
            offset: offset + mapping_offset,
        })
    }

    Ok(())
}
