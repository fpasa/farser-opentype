use super::*;

pub(crate) fn parse_loca_table(
    mut reader: &mut (impl BufRead + Seek),
    offset: u32,
    font: &mut Font,
    font_data: &mut ParserFontData,
    config: &ParseConfig,
) -> Result<(), Error> {
    reader.seek(SeekFrom::Start(offset as u64));

    if font_data.offset_type == OffsetType::Short {
        for _ in 0..(font.num_glyphs + 1) {
            font_data
                .glyph_offsets
                .push(read_u16(&mut reader)? as u32 * 2);
        }
    } else {
        for _ in 0..(font.num_glyphs + 1) {
            font_data.glyph_offsets.push(read_u32(&mut reader)?);
        }
    }

    Ok(())
}
