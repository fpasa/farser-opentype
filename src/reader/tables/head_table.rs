use super::*;

pub(crate) fn parse_head_table(
    mut reader: &mut (impl BufRead + Seek),
    offset: u32,
    font: &mut Font,
    font_data: &mut ParserFontData,
    config: &ParseConfig,
) -> Result<(), Error> {
    reader.seek(SeekFrom::Start(offset as u64));

    let major_version = read_u16(&mut reader)?;
    if major_version != 1 {
        return Err(Error::ParseError(format!(
            "Unsupported head table major version {}.",
            major_version
        )));
    }

    let minor_version = read_u16(&mut reader)?;
    if minor_version != 0 {
        font.add_warning(
            &format!(
                "Unsupported minor version: head table has version {}.{} \
            but only version up to 1.0 are supported. \
            Some features might be missing",
                major_version, minor_version
            ),
            config,
        );
    }

    font.revision = read_fixed(&mut reader)?;

    // checkSumAdjustment is currently unsupported, see #1
    let checkSumAdjustment = read_u32(&mut reader)?;

    let magic_number = read_tag(&mut reader)?;
    if magic_number != tags::HEAD_MAGIC_NUMBER {
        font.add_warning(
            &format!(
                "Magic number does not match for table head: {:?} should be {:?}",
                magic_number,
                tags::HEAD_MAGIC_NUMBER,
            ),
            config,
        );
    }

    // TODO
    let flags = read_u16(&mut reader);

    font.units_per_em = read_u16(&mut reader)?;
    font.created = read_date(&mut reader)?;
    font.modified = read_date(&mut reader)?;

    font.bounds = read_bounds(&mut reader)?;

    // TODO
    let mac_Style = read_u16(&mut reader)?;

    font.smallest_readable_size_in_px = read_u16(&mut reader)?;
    font.font_direction_hint = match read_i16(&mut reader)? {
        -2 => FontDirectionHint::RightToLeftNeutral,
        -1 => FontDirectionHint::RightToLeft,
        0 => FontDirectionHint::Mixed,
        1 => FontDirectionHint::LeftToRight,
        2 => FontDirectionHint::LeftToRightNeutral,
        val => {
            return Err(Error::ParseError(format!(
                "Invalid font directionality '{}'.",
                val
            )))
        }
    };

    font_data.offset_type = match read_i16(&mut reader)? {
        0 => OffsetType::Short,
        1 => OffsetType::Long,
        val => return Err(Error::ParseError(format!("Invalid offset type '{}'.", val))),
    };

    Ok(())
}
