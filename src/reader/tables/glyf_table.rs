use super::*;
use crate::Error::ParseError;

const FLAG_ON_CURVE_POINT: u8 = 0x01;
const FLAG_X_SHORT_VECTOR: u8 = 0x02;
const FLAG_Y_SHORT_VECTOR: u8 = 0x04;
const FLAG_REPEAT_FLAG: u8 = 0x08;
const FLAG_X_IS_SAME_OR_POSITIVE_X_SHORT_VECTOR: u8 = 0x10;
const FLAG_Y_IS_SAME_OR_POSITIVE_Y_SHORT_VECTOR: u8 = 0x20;
const FLAG_OVERLAP_SIMPLE: u8 = 0x30;

pub(crate) fn parse_glyf_table(
    mut reader: &mut (impl BufRead + Seek),
    offset: u32,
    font: &mut Font,
    font_data: &mut ParserFontData,
    config: &ParseConfig,
) -> Result<(), Error> {
    if font.num_glyphs != font_data.glyph_offsets.len() as u16 - 1 {
        return Err(ParseError(format!(
            "Invalid data, num_glyphs in the head table is {}, \
            but there are {} (num_glyphs+1) offsets in the loca table.",
            font.num_glyphs,
            font_data.glyph_offsets.len()
        )));
    }

    for i in 0..(font.num_glyphs as usize) {
        let glyph_offset = font_data.glyph_offsets[i];
        if font_data.glyph_offsets[i + 1] < glyph_offset {
            return Err(ParseError(
                "Glyph indices in the loca table are not monotonically increasing.".to_string(),
            ));
        }
        let glyph_length = font_data.glyph_offsets[i + 1] - glyph_offset;

        let glyph = parse_glyph(&mut reader, offset + glyph_offset, glyph_length)?;
        font.glyps.push(glyph);
    }

    Ok(())
}

fn parse_glyph(
    mut reader: &mut (impl BufRead + Seek),
    offset: u32,
    length: u32,
) -> Result<Glyph, Error> {
    if length == 0 {
        // glyph has not outline (e.g. space character)
        return Ok(Glyph::default());
    }

    reader.seek(SeekFrom::Start(offset as u64));

    let num_contours = read_i16(&mut reader)?;
    let bounds = read_bounds(&mut reader)?;

    // TODO: will this ever happen? Do we need to handle it specially?
    if length != 0 && num_contours == 0 {
        panic!("TODO -> (1) How to handle this case? Does it happen?");
    }

    if num_contours >= 0 {
        parse_simple_glyph(&mut reader, bounds, num_contours)
    } else {
        parse_composite_glyph(&mut reader, bounds, num_contours)
    }
}

fn parse_simple_glyph(
    mut reader: &mut (impl BufRead + Seek),
    bounds: Bounds,
    num_contours: i16,
) -> Result<Glyph, Error> {
    // TODO: will this ever happen? Do we need to handle it specially?
    if num_contours == 0 {
        panic!("TODO -> (2) How to handle this case? Does it happen?");
    }

    let contour_end_points = read_vec_u16(&mut reader, num_contours as usize)?;
    let instructions_length = read_u16(&mut reader)?;
    let instructions = read_vec_u8(&mut reader, instructions_length as usize)?;

    let num_points = *contour_end_points.last().unwrap() as u32 + 1;
    let flags = parse_flags(&mut reader, num_points)?;
    let coordinates = parse_coordinates(&mut reader, &flags)?;

    let mut contours = Vec::new();
    let mut prev_point = 0;
    for end_point in contour_end_points {
        contours.push(Contour {
            points: coordinates[prev_point..end_point as usize]
                .iter()
                .map(|coord| coord.clone())
                .collect(),
        });
        prev_point = end_point as usize;
    }

    Ok(Glyph { bounds, contours })
}

fn parse_flags(mut reader: &mut (impl BufRead + Seek), num_points: u32) -> Result<Vec<u8>, Error> {
    let mut found_points = 0;
    let mut flags = Vec::new();
    loop {
        let flag = read_u8(&mut reader)?;

        // flags can use packed representation, so unpack if necessary
        let mut num_logical_flags = 1;
        if flag & FLAG_REPEAT_FLAG != 0 {
            num_logical_flags += read_u8(&mut reader)? as u32;
        }

        for _ in 0..num_logical_flags {
            flags.push(flag);
        }
        found_points += num_logical_flags;

        if found_points == num_points {
            break;
        }
    }
    Ok(flags)
}

fn parse_coordinates(
    mut reader: &mut (impl BufRead + Seek),
    flags: &Vec<u8>,
) -> Result<Vec<Coordinate>, Error> {
    let mut coords = Vec::new();

    let x = parse_coordinates_value(
        &mut reader,
        flags,
        FLAG_X_SHORT_VECTOR,
        FLAG_X_IS_SAME_OR_POSITIVE_X_SHORT_VECTOR,
    )?;
    let y = parse_coordinates_value(
        &mut reader,
        flags,
        FLAG_Y_SHORT_VECTOR,
        FLAG_Y_IS_SAME_OR_POSITIVE_Y_SHORT_VECTOR,
    )?;
    for i in 0..flags.len() {
        coords.push(Coordinate { x: x[i], y: y[i] });
    }

    Ok(coords)
}

fn parse_coordinates_value(
    mut reader: &mut (impl BufRead + Seek),
    flags: &Vec<u8>,
    short_flag: u8,
    same_or_positive_flag: u8,
) -> Result<Vec<i32>, Error> {
    let mut values = Vec::new();

    let mut val = 0;
    for flag in flags.iter() {
        val = parse_coordinate_value(&mut reader, *flag, short_flag, same_or_positive_flag, val)?;
        values.push(val);
    }

    Ok(values)
}

fn parse_coordinate_value(
    mut reader: &mut (impl BufRead + Seek),
    flag: u8,
    short_flag: u8,
    same_or_positive_flag: u8,
    prev: i32,
) -> Result<i32, Error> {
    if flag & same_or_positive_flag != 0 {
        return Ok(prev);
    }

    let mut x = 0;
    if flag & short_flag != 0 {
        x = read_u8(&mut reader)? as i32;
        if flag & same_or_positive_flag != 0 {
            x *= -1;
        }
    } else {
        x = read_i16(&mut reader)? as i32;
    }

    Ok(prev + x)
}

fn parse_composite_glyph(
    mut reader: &mut (impl BufRead + Seek),
    bounds: Bounds,
    num_contours: i16,
) -> Result<Glyph, Error> {
    Ok(Glyph::default())
}
