// This module implements the logic to parse the different font file formats
// into the structures defined in other modules.

use super::*;

use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::{BufRead, BufReader, Seek, SeekFrom};
use std::path::Path;
use std::str;

mod helpers;
use self::helpers::*;
mod tables;
use self::tables::*;
mod font_data;
use self::font_data::*;
mod tags;
mod validation;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct ParseConfig {
    pub check_integrity: bool,
    pub print_warnings: bool,
}

impl Default for ParseConfig {
    fn default() -> Self {
        Self {
            check_integrity: true,
            print_warnings: true,
        }
    }
}

pub fn parse_default(path: impl AsRef<Path>) -> Result<FontCollection, Error> {
    parse(path, &ParseConfig::default())
}

pub fn parse(path: impl AsRef<Path>, config: &ParseConfig) -> Result<FontCollection, Error> {
    let path = path.as_ref();
    match path.extension() {
        Some(ext) => {
            if ext != "ttf" && ext != "otf" && ext != "ttc" && ext != "otc" {
                return Err(Error::ValueError(
                    "Only TTF/OTF/TTC/OTC files are supported.".to_string(),
                ));
            }
        }
        None => {
            return Err(Error::ValueError(
                "Only TTF/OTF/TTC/OTC files are supported.".to_string(),
            ))
        }
    }

    let mut reader = open_file(path)?;
    let font_collection = parse_file(&mut reader, config)?;
    Ok(font_collection)
}

fn parse_file(
    reader: &mut (impl BufRead + Seek),
    config: &ParseConfig,
) -> Result<FontCollection, Error> {
    let tag = read_tag(reader)?;

    if tag == tags::TYPE_TRUETYPE || tag == tags::TYPE_CFF {
        reader.seek(SeekFrom::Start(0));
        Ok(FontCollection {
            fonts: vec![parse_font(reader, config)?],
        })
    } else if tag == tags::TYPE_COLLECTION {
        Ok(parse_collection(reader, config)?)
    } else {
        Err(Error::ParseError(
            "Unsupported or invalid collection tag.".to_string(),
        ))
    }
}

fn parse_collection(
    mut reader: &mut (impl BufRead + Seek),
    config: &ParseConfig,
) -> Result<FontCollection, Error> {
    let version = read_tag(reader)?;
    let num_fonts = read_u32(reader)?;

    if num_fonts == 0 {
        return Err(Error::ParseError(
            "Font collection does not contain any fonts.".to_string(),
        ));
    }

    let mut offsets = Vec::new();
    for _ in 0..num_fonts {
        offsets.push(read_u32(reader)?);
    }

    let mut fonts = Vec::new();
    for offset in offsets {
        reader.seek(SeekFrom::Start(offset as u64));
        fonts.push(parse_font(reader, config)?);
    }

    match version {
        tags::COLLECTION_VERSION_1_0 => Ok(FontCollection { fonts }),
        tags::COLLECTION_VERSION_2_0 => Ok(FontCollection { fonts }),
        _ => Err(Error::ParseError(
            "Unsupported or invalid font collection version.".to_string(),
        )),
    }
}

fn parse_font(mut reader: &mut (impl BufRead + Seek), config: &ParseConfig) -> Result<Font, Error> {
    let tag = read_tag(reader)?;
    let num_tables = read_u16(reader)?;
    let search_range = read_u16(reader)?;
    let entry_selector = read_u16(reader)?;
    let range_shift = read_u16(reader)?;

    let mut font = Font::default();
    let mut font_data = ParserFontData::default();

    font.outline_type = match tag {
        tags::TYPE_TRUETYPE => FontOutlineType::TrueType,
        tags::TYPE_CFF => FontOutlineType::CompactFontFormat,
        _ => {
            return Err(Error::ParseError(
                "Unsupported or invalid font tag.".to_string(),
            ))
        }
    };

    // First read all table headers in the file
    let mut table_headers = HashMap::new();
    for _ in 0..num_tables {
        let table_header = parse_table_header(&mut reader)?;
        table_headers.insert(table_header.tag, table_header);
    }

    // Check that the required reader.tables exist
    validation::check_required_tables(&tag, &table_headers.keys().clone().collect());

    // Then read reader.tables. Reading reader.tables requires seeking around, and than the
    // position is lost and it's not possible to read other table headers.
    // For these reason, all headers are read immediately.
    if config.check_integrity {
        for table_header in table_headers.values() {
            if table_header.tag == tags::TABLE_head {
                continue;
            }

            validation::check_table_integrity(
                &mut reader,
                table_header.check_sum,
                table_header.offset,
                table_header.length,
            );
        }
    }

    let head_table = &table_headers[&tags::TABLE_head];
    parse_head_table(
        &mut reader,
        head_table.offset,
        &mut font,
        &mut font_data,
        config,
    );
    let maxp_table = &table_headers[&tags::TABLE_maxp];
    parse_maxp_table(
        &mut reader,
        maxp_table.offset,
        &mut font,
        &mut font_data,
        config,
    );

    match font.outline_type {
        FontOutlineType::TrueType => {
            let cmap_table = &table_headers[&tags::TABLE_cmap];
            parse_cmap_table(
                &mut reader,
                cmap_table.offset,
                &mut font,
                &mut font_data,
                config,
            );
            let loca_table = &table_headers[&tags::TABLE_loca];
            parse_loca_table(
                &mut reader,
                loca_table.offset,
                &mut font,
                &mut font_data,
                config,
            );
            // let glyf_table = &table_headers[&tags::TABLE_glyf];
            // parse_glyf_table(
            //     &mut reader,
            //     glyf_table.offset,
            //     &mut font,
            //     &mut font_data,
            //     config,
            // );
        }
        FontOutlineType::CompactFontFormat => {}
    }

    Ok(font)
}

struct TableHeader {
    tag: tags::Tag,
    check_sum: u32,
    offset: u32,
    length: u32,
}

fn parse_table_header(mut reader: &mut (impl BufRead + Seek)) -> Result<TableHeader, Error> {
    let tag = read_tag(reader)?;
    let check_sum = read_u32(reader)?;
    let offset = read_u32(reader)?;
    let length = read_u32(reader)?;

    // Check that the table tag is valid
    validation::check_table_tag(&tag)?;

    Ok(TableHeader {
        tag,
        check_sum,
        offset,
        length,
    })
}

#[cfg(test)]
mod tests;
