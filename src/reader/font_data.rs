#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) enum OffsetType {
    Short,
    Long,
}

// This struct is for all data that is needed to properly parse the font,
// but we don't want to expose outside the parser since it's irrelevant
// for anything but the parser
#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) struct ParserFontData {
    pub offset_type: OffsetType,
    pub glyph_offsets: Vec<u32>,
}

impl Default for ParserFontData {
    fn default() -> Self {
        // Some field are set to non-zero value to avoid wrapping them in options
        // which make the usage verbose. All values are overwritten by the parser.
        Self {
            offset_type: OffsetType::Short,
            glyph_offsets: vec![],
        }
    }
}
