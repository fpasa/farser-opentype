pub(crate) type Tag = [u8; 4];

pub(crate) const TYPE_TRUETYPE: Tag = [0, 1, 0, 0];
// CFF stands for Compact Font Format
pub(crate) const TYPE_CFF: Tag = [0x4F, 0x54, 0x54, 0x4F]; // OTTO
pub(crate) const TYPE_COLLECTION: Tag = [116, 116, 99, 102]; // ttfc

pub(crate) const COLLECTION_VERSION_1_0: Tag = [0, 1, 0, 0];
pub(crate) const COLLECTION_VERSION_2_0: Tag = [0, 2, 0, 0];

pub(crate) const TABLE_avar: Tag = [97, 118, 97, 114];
pub(crate) const TABLE_BASE: Tag = [66, 65, 83, 69];
pub(crate) const TABLE_CBDT: Tag = [67, 66, 68, 84];
pub(crate) const TABLE_CBLC: Tag = [67, 66, 76, 67];
pub(crate) const TABLE_CFF: Tag = [67, 70, 70, 32];
pub(crate) const TABLE_CFF2: Tag = [67, 70, 70, 50];
pub(crate) const TABLE_cmap: Tag = [99, 109, 97, 112];
pub(crate) const TABLE_COLR: Tag = [67, 79, 76, 82];
pub(crate) const TABLE_CPAL: Tag = [67, 80, 65, 76];
pub(crate) const TABLE_cvar: Tag = [99, 118, 97, 114];
pub(crate) const TABLE_cvt: Tag = [99, 118, 116, 32];
pub(crate) const TABLE_DSIG: Tag = [68, 83, 73, 71];
pub(crate) const TABLE_EBDT: Tag = [69, 66, 68, 84];
pub(crate) const TABLE_EBLC: Tag = [69, 66, 76, 67];
pub(crate) const TABLE_EBSC: Tag = [69, 66, 83, 67];
pub(crate) const TABLE_fpgm: Tag = [102, 112, 103, 109];
pub(crate) const TABLE_fvar: Tag = [102, 118, 97, 114];
pub(crate) const TABLE_gasp: Tag = [103, 97, 115, 112];
pub(crate) const TABLE_GDEF: Tag = [71, 68, 69, 70];
pub(crate) const TABLE_glyf: Tag = [103, 108, 121, 102];
pub(crate) const TABLE_GPOS: Tag = [71, 80, 79, 83];
pub(crate) const TABLE_GSUB: Tag = [71, 83, 85, 66];
pub(crate) const TABLE_gvar: Tag = [103, 118, 97, 114];
pub(crate) const TABLE_hdmx: Tag = [104, 100, 109, 120];
pub(crate) const TABLE_head: Tag = [104, 101, 97, 100];
pub(crate) const TABLE_hhea: Tag = [104, 104, 101, 97];
pub(crate) const TABLE_hmtx: Tag = [104, 109, 116, 120];
pub(crate) const TABLE_HVAR: Tag = [72, 86, 65, 82];
pub(crate) const TABLE_JSTF: Tag = [74, 83, 84, 70];
pub(crate) const TABLE_kern: Tag = [107, 101, 114, 110];
pub(crate) const TABLE_loca: Tag = [108, 111, 99, 97];
pub(crate) const TABLE_LTSH: Tag = [76, 84, 83, 72];
pub(crate) const TABLE_MATH: Tag = [77, 65, 84, 72];
pub(crate) const TABLE_maxp: Tag = [109, 97, 120, 112];
pub(crate) const TABLE_MERG: Tag = [77, 69, 82, 71];
pub(crate) const TABLE_meta: Tag = [109, 101, 116, 97];
pub(crate) const TABLE_MVAR: Tag = [77, 86, 65, 82];
pub(crate) const TABLE_name: Tag = [110, 97, 109, 101];
pub(crate) const TABLE_OS_2: Tag = [79, 83, 47, 50];
pub(crate) const TABLE_PCLT: Tag = [80, 67, 76, 84];
pub(crate) const TABLE_post: Tag = [112, 111, 115, 116];
pub(crate) const TABLE_prep: Tag = [112, 114, 101, 112];
pub(crate) const TABLE_sbix: Tag = [115, 98, 105, 120];
pub(crate) const TABLE_STAT: Tag = [83, 84, 65, 84];
pub(crate) const TABLE_SVG: Tag = [83, 86, 71, 32];
pub(crate) const TABLE_VDMX: Tag = [86, 68, 77, 88];
pub(crate) const TABLE_vhea: Tag = [118, 104, 101, 97];
pub(crate) const TABLE_vmtx: Tag = [118, 109, 116, 120];
pub(crate) const TABLE_VORG: Tag = [86, 79, 82, 71];
pub(crate) const TABLE_VVAR: Tag = [86, 86, 65, 82];

pub(crate) const TABLE_TAGS: &[Tag] = &[
    TABLE_avar, TABLE_BASE, TABLE_CBDT, TABLE_CBLC, TABLE_CFF, TABLE_CFF2, TABLE_cmap, TABLE_COLR,
    TABLE_CPAL, TABLE_cvar, TABLE_cvt, TABLE_DSIG, TABLE_EBDT, TABLE_EBLC, TABLE_EBSC, TABLE_fpgm,
    TABLE_fvar, TABLE_gasp, TABLE_GDEF, TABLE_glyf, TABLE_GPOS, TABLE_GSUB, TABLE_gvar, TABLE_hdmx,
    TABLE_head, TABLE_hhea, TABLE_hmtx, TABLE_HVAR, TABLE_JSTF, TABLE_kern, TABLE_loca, TABLE_LTSH,
    TABLE_MATH, TABLE_maxp, TABLE_MERG, TABLE_meta, TABLE_MVAR, TABLE_name, TABLE_OS_2, TABLE_PCLT,
    TABLE_post, TABLE_prep, TABLE_sbix, TABLE_STAT, TABLE_SVG, TABLE_VDMX, TABLE_vhea, TABLE_vmtx,
    TABLE_VORG, TABLE_VVAR,
];

pub(crate) const HEAD_MAGIC_NUMBER: Tag = [0x5F, 0x0F, 0x3C, 0xF5];
