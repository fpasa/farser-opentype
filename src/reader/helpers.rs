use super::*;

use chrono::{DateTime, Duration, TimeZone, Utc};
use std::io::Read;
use std::mem;
use std::ops::Add;

pub(crate) fn open_file(filename: impl AsRef<Path>) -> Result<BufReader<File>, Error> {
    let file = File::open(filename.as_ref())?;
    let reader = BufReader::new(file);
    Ok(reader)
}

pub(crate) fn read_tag(reader: &mut impl BufRead) -> Result<tags::Tag, Error> {
    let mut tag = [0; 4];
    reader.read_exact(&mut tag)?;
    Ok(tag)
}

pub(crate) fn read_vec_u8(reader: &mut impl BufRead, n: usize) -> Result<Vec<u8>, Error> {
    let mut bytes = Vec::with_capacity(n);
    let mut buffer = [0; 1];
    for _ in 0..n {
        reader.read_exact(&mut buffer)?;
        bytes.push(buffer[0]);
    }
    Ok(bytes)
}

pub(crate) fn read_vec_u16(reader: &mut impl BufRead, n: usize) -> Result<Vec<u16>, Error> {
    let mut bytes = Vec::with_capacity(n);
    let mut buffer = [0; 2];
    for _ in 0..n {
        reader.read_exact(&mut buffer)?;
        bytes.push(u16::from_be_bytes(buffer));
    }
    Ok(bytes)
}

pub(crate) fn read_u8(reader: &mut impl BufRead) -> Result<u8, Error> {
    let mut num = [0; 1];
    reader.read_exact(&mut num)?;
    Ok(num[0])
}

pub(crate) fn read_u16(reader: &mut impl BufRead) -> Result<u16, Error> {
    let mut num = [0; 2];
    reader.read_exact(&mut num)?;
    Ok(u16::from_be_bytes(num))
}

pub(crate) fn read_i16(reader: &mut impl BufRead) -> Result<i16, Error> {
    let mut num = [0; 2];
    reader.read_exact(&mut num)?;
    Ok(i16::from_be_bytes(num))
}

pub(crate) fn read_u32(reader: &mut impl BufRead) -> Result<u32, Error> {
    let mut num = [0; 4];
    reader.read_exact(&mut num)?;
    Ok(u32::from_be_bytes(num))
}

pub(crate) fn read_date(reader: &mut impl BufRead) -> Result<DateTime<Utc>, Error> {
    let mut num = [0; 8];
    reader.read_exact(&mut num)?;
    let seconds_since_1904 = i64::from_be_bytes(num);
    let datetime = Utc.ymd(1904, 1, 1).and_hms(0, 0, 0) + Duration::seconds(seconds_since_1904);
    Ok(datetime)
}

pub(crate) fn read_bounds(mut reader: &mut impl BufRead) -> Result<Bounds, Error> {
    let x_min = read_i16(&mut reader)?;
    let y_min = read_i16(&mut reader)?;
    let x_max = read_i16(&mut reader)?;
    let y_max = read_i16(&mut reader)?;
    let width = (x_max - x_min) as u16;
    let height = (y_max - y_min) as u16;
    Ok(Bounds {
        x_min,
        y_min,
        x_max,
        y_max,
        width,
        height,
    })
}

pub(crate) fn read_fixed(mut reader: &mut impl BufRead) -> Result<Version, Error> {
    Ok(Version {
        major: read_u16(&mut reader)?,
        // Total bullshit conversion, maybe there's a better way?
        minor: (read_u16(&mut reader)? as f32 / 65.536).round() as u16,
    })
}
