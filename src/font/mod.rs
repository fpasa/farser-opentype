use super::*;

use chrono::{DateTime, Utc};

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum FontOutlineType {
    TrueType,
    CompactFontFormat,
}

#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub struct Version {
    pub major: u16,
    pub minor: u16,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum FontDirectionHint {
    Mixed,
    LeftToRight,
    LeftToRightNeutral,
    RightToLeft,
    RightToLeftNeutral,
}

#[derive(Debug, Clone)]
pub struct Font {
    pub outline_type: FontOutlineType,
    pub revision: Version,
    pub units_per_em: u16,
    pub created: DateTime<Utc>,
    pub modified: DateTime<Utc>,
    pub bounds: Bounds,
    pub smallest_readable_size_in_px: u16,
    pub font_direction_hint: FontDirectionHint,
    pub num_glyphs: u16,
    pub max_points: u16,
    pub max_contours: u16,
    pub max_composite_points: u16,
    pub max_composite_contours: u16,
    pub max_zones: u16,
    pub max_twilight_points: u16,
    pub max_storage: u16,
    pub max_max_function_defs: u16,
    pub max_instruction_defs: u16,
    pub max_stack_elements: u16,
    pub max_size_of_instruction: u16,
    pub max_component_elements: u16,
    pub max_component_depth: u16,
    mappings: Vec<Mapping>,
    glyps: Vec<Glyph>,
    warnings: Vec<Warning>,
    // reader.tables: Vec<Table>,
}

impl Default for Font {
    fn default() -> Self {
        // Some field are set to non-zero value to avoid wrapping them in options
        // which make the usage verbose. All values are overwritten by the parser.
        Self {
            outline_type: FontOutlineType::TrueType,
            revision: Version::default(),
            units_per_em: 0,
            created: Utc::now(),
            modified: Utc::now(),
            bounds: Bounds::default(),
            smallest_readable_size_in_px: 0,
            font_direction_hint: FontDirectionHint::Mixed,
            num_glyphs: 0,
            max_points: 0,
            max_contours: 0,
            max_composite_points: 0,
            max_composite_contours: 0,
            max_zones: 0,
            max_twilight_points: 0,
            max_storage: 0,
            max_max_function_defs: 0,
            max_instruction_defs: 0,
            max_stack_elements: 0,
            max_size_of_instruction: 0,
            max_component_elements: 0,
            max_component_depth: 0,
            mappings: vec![],
            glyps: vec![],
            warnings: vec![],
        }
    }
}

impl Font {
    pub fn add_warning(&mut self, message: &str, config: &ParseConfig) {
        self.warnings.push(Warning {
            message: message.to_string(),
        });

        if config.print_warnings {
            println!("{}", message);
        }
    }

    pub fn get_warnings(&self) -> &Vec<Warning> {
        &self.warnings
    }

    pub(crate) fn add_mapping(&mut self, mapping: Mapping) {
        self.mappings.push(mapping);
    }
}
