#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub struct Bounds {
    pub x_min: i16,
    pub y_min: i16,
    pub x_max: i16,
    pub y_max: i16,
    pub width: u16,
    pub height: u16,
}
