use super::*;

/// Represent a warning message.
///
/// When parsing font files, the parser might emit warnings about unsupported
/// features or malformed data, which are not serious enough to cause errors,
/// but might cause certain features or data to be unavailable.
#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub struct Warning {
    pub message: String,
}
