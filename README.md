<div align="center">

![farser-opentype](logo.png)
</div>

# farser-opentype

A pure-rust font parser library supporting all OpenType and TrueType
formats, including collection types (.ttf/.otf/.ttc/.otc).

`farser-opentype` is not a font rasterization or text layout library,
it just provides a way to open and parse OpenType font files into a
structured, high-level and normalized format.

OpenType fonts are very complex in order to support many scripts,
uses cases and advanced typography features, but also for historical
and backward compatibility reasons. `farser-opentype` tries to normalize
all these features into a standardized format so that libraries or applications
using it do not need to know the intricacies and can instead concentrate
on using the data instead.

## Goals

- Support most OpenType features, including font collections.
- Hide internal formats such as the difference between TrueType and
  Compact Font Format outlines.
- Provide clear errors for non-compliant files.

## Non-Goals

- Provide support to formats outside TrueType and OpenType. This in case would
  be a job for a linked crate.
- Have anything to do with rasterization and other features. The library
  should just expose the font data.

## Status

The crate is in early development, it won't be production-ready until version 1.
This means that the exposed structure might change until that version,
allowing us to experiment with the data structure that makes most sense.
Unnecessary changes will be avoided if possible, to avoid breaking
dependent packages.

Currently, the following OpenType features are supported:

- Basic support for font collections (reading the number of fonts and their data).
- Some basic fields.
- TrueType outline data.

Notable features that are WIP:

- Metrics.
- Compact Font Format outlines.
- Other metadata fields.
- Variable fonts.

## Roadmap

1. Expose (almost) all OpenType features.
2. (maybe) support WOFF fonts, since they're basically compressed OpenType fonts
   (could be another package).
3. (maybe) Support font writing.

## Contributions

Contributions are very welcome. Before contributing, please submit an issue or
contact me in the [Rust Community Discord](https://bit.ly/rust-community)
(my username is `@frapa`), so that we avoid duplicating efforts.

Ways to contribute/support the library:

- Report bugs.
- Add new features or fix bugs (have a look  at the issues for ideas).
- Discuss improvements.
- Use the library in your own programs (would be glad to ear).